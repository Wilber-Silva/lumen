FROM php:7.4-cli

RUN curl -sS https://getcomposer.org/installer | php -- \
        --install-dir=/usr/bin --filename=composer

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "zip"]
RUN ["apt-get", "install", "-y", "unzip"]

COPY . /usr/src
WORKDIR /usr/src

RUN composer install
#CMD [ "php", "artisan", "migrate" ]

EXPOSE 9000

#MD php -s localhost:9000 -t public
#CMD [ "php","artisan", "start" ]
