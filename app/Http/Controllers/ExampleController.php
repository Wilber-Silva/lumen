<?php

namespace App\Http\Controllers;

class ExampleController extends Controller
{
    private $collection;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = collect([
            "boleto" => collect([
                (object)  [
                    'idProposta' => 100,
                    'valorEquipamentos' => 10000.40,
                    'valorMaoObra' => 20000.12,
                    'valorOpex' => 10000.34,
                    'valorImposto' => 3000.76,
                    'valorMargem' => 40000.02,
                    'parcelas' => 4,
                    'pagas' => 1,
                    'percentuais' => collect([30,23.33,23.33,23.33])
                ],
                (object)  [
                    'idProposta' => 103,
                    'valorEquipamentos' => 22300.40,
                    'valorMaoObra' => 33500.12,
                    'valorOpex' => 44060.34,
                    'valorImposto' => 5530.76,
                    'valorMargem' => 66400.02,
                    'parcelas' => 4,
                    'pagas' => 1,
                    'percentuais' => collect([10,30,30,30])
                ],
                (object)  [
                    'idProposta' => 104,
                    'valorEquipamentos' => 122300.40,
                    'valorMaoObra' => 133500.12,
                    'valorOpex' => 144060.34,
                    'valorImposto' => 15530.76,
                    'valorMargem' => 166400.02,
                    'parcelas' => 4,
                    'pagas' => 1,
                    'percentuais' => collect([15,28,40,17])
                ],
                (object)  [
                    'idProposta' => 106,
                    'valorEquipamentos' => 12300.40,
                    'valorMaoObra' => 13500.12,
                    'valorOpex' => 24060.34,
                    'valorImposto' => 3530.76,
                    'valorMargem' => 56400.02,
                    'parcelas' => 4,
                    'pagas' => 1,
                    'percentuais' => collect([55,10,17.5,17.5])
                ]
            ]),
            "cartao" => collect([
                (object)  [
                    'idProposta' => 101,
                    'valorEquipamentos' => 20000.40,
                    'valorMaoObra' => 30000.12,
                    'valorOpex' => 40000.34,
                    'valorImposto' => 5000.76,
                    'valorMargem' => 60000.02,
                    'parcelas' => 12,
                    'pagas' => 2
                ],
                (object)  [
                    'idProposta' => 102,
                    'valorEquipamentos' => 20300.40,
                    'valorMaoObra' => 30500.12,
                    'valorOpex' => 40060.34,
                    'valorImposto' => 5030.76,
                    'valorMargem' => 60400.02,
                    'parcelas' => 10,
                    'pagas' => 1
                ],
                (object)  [
                    'idProposta' => 105,
                    'valorEquipamentos' => 82300.40,
                    'valorMaoObra' => 73500.12,
                    'valorOpex' => 104060.34,
                    'valorImposto' => 11530.76,
                    'valorMargem' => 126400.02,
                    'parcelas' => 10,
                    'pagas' => 5
                ]
            ])
        ]);
    }
    protected function sumTotal($item) {
        return $item->valorEquipamentos + $item->valorMaoObra;
    }
    public function truncate($number) {
        $prec = 2;
        // o number_format tava retornando um valor nada a ver então fiz na mão mesmo
        return (float) sprintf( "%.".$prec."f", floor( $number*pow( 10, $prec ) )/pow( 10, $prec ) );
    }
    public function test(){
        return [
            "totals" => [
                "boleto" => $this->collection["boleto"]->reduce(function ($carry, $item) {
                    if (!$carry) return $item;

                    return (object) [
                        'valorEquipamentos' => $carry->valorEquipamentos + $item->valorEquipamentos,
                        'valorMaoObra' => $this->truncate($carry->valorMaoObra + $item->valorMaoObra),
                        'valorOpex' => $carry->valorOpex + $item->valorOpex,
                        'valorImposto' => $carry->valorImposto + $item->valorImposto,
                        'valorMargem' => $carry->valorMargem + $item->valorMargem,
                        'parcelas' => $carry->parcelas + $item->parcelas,
                        'pagas' => $carry->pagas + $item->pagas
                    ];
                }),
                "cartao" => $this->collection["cartao"]->reduce(function ($carry, $item) {
                    if (!$carry) return $item;

                    return (object) [
                        'valorEquipamentos' => $carry->valorEquipamentos + $item->valorEquipamentos,
                        'valorMaoObra' => $carry->valorMaoObra + $item->valorMaoObra,
                        'valorOpex' => $carry->valorOpex + $item->valorOpex,
                        'valorImposto' => $carry->valorImposto + $item->valorImposto,
                        'valorMargem' => $carry->valorMargem + $item->valorMargem,
                        'parcelas' => $carry->parcelas + $item->parcelas,
                        'pagas' => $carry->pagas + $item->pagas
                    ];
                })
            ],
            "aggregation" => [
                "boleto" => $this->collection["boleto"]->reduce(function ($carry, $item) {
                    $item->percentualTotal = $item->percentuais->reduce(function($carry, $item) { return $carry + $item; });
                    $item->valorTotal = $this->truncate($this->sumTotal($item));
                    $item->valorParcela =  $this->truncate($item->valorTotal / $item->parcelas);
                    $item->valorTotalPago = $this->truncate($item->valorTotal * ($item->percentualTotal / 100));
                    $item->valorTotalPagar = $this->truncate($item->valorTotal - $item->valorTotalPago);

                    if (!$carry) return $item;

                    return (object) [
                        'valorTotalPago' => $carry->valorTotalPago + $item->valorTotalPago,
                        'valorTotalPagar' => $carry->valorTotalPagar + $item->valorTotalPagar
                    ];
                }),
                "cartao" => $this->collection["cartao"]->reduce(function ($carry, $item) {
                    $item->valorTotal = $this->truncate($this->sumTotal($item));
                    $item->valorParcela = $this->truncate($item->valorTotal / $item->parcelas);
                    $item->valorTotalPago = $this->truncate($item->valorParcela * $item->pagas);
                    $item->valorTotalPagar = $this->truncate($item->valorTotal - $item->valorTotalPago);

                    if (!$carry) return $item;

                    return (object) [
                        'valorTotalPago' => $carry->valorTotalPago + $item->valorTotalPago,
                        'valorTotalPagar' => $carry->valorTotalPagar + $item->valorTotalPagar
                    ];
                })
            ],
            "collection" => $this->collection
        ];
    }
}
